package customer.Service;

import java.util.Scanner;

import customer.Dao.CustomerAuthentication;
import customer.Dao.CustomerAuthenticationImpl;
import customer.Pojo.Customer;

public class CustomerServiceImpl implements CustomerService {

	// add necessary objects
	CustomerAuthentication refCustAuthentication;
	Scanner refScanner;
	Customer refCust;
	@Override
	public void obtaincustomerinput() {
		
		// initialise scanner
		refScanner = new Scanner(System.in);
		
		
		// ask customer for customer ID
		System.out.println("Enter customer ID:");
		String customerID = refScanner.next();
		
		// ask customer for password
		System.out.println("Enter password:");
		String password = refScanner.next();
		
		// initialise the customer and customerauthentication object, then the authentication object
		refCust = new Customer();
		refCustAuthentication = new CustomerAuthenticationImpl();
		refCust.setCustomerID(customerID);
		refCust.setPassword(password);
		refCustAuthentication.authentication(refCust);
		
	}
	
}
