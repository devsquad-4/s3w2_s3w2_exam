package customer.Dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import customer.Pojo.Customer;

import customer.Utility.DBUtility;

public class CustomerAuthenticationImpl implements CustomerAuthentication {

	Connection refConnection = null;
	PreparedStatement refPreparedStatement =null;
	ResultSet rs = null;
	
	@Override
	public void authentication(Customer refCustomer) {
		try {
			// number of results
			int row = 0;
			
			// obtain the connection
			refConnection = DBUtility.getConnection();
			
			
			// mysql insert query
			String sqlQuery = "select * from customer where customerID=? and password=?";
			
			// create the mysql insert preparedstatement
			refPreparedStatement = refConnection.prepareStatement(sqlQuery);
			refPreparedStatement.setString(1, refCustomer.getCustomerID());
			refPreparedStatement.setString(2, refCustomer.getPassword());
			
			// execute the query
			rs = refPreparedStatement.executeQuery();
			
			while(rs.next()) {
				row++;
			}
			
			
			
			// check whether the data exists
			if(row == 0) {
				System.out.println("Login failed.");
			}else {
				System.out.println("Login Successful");
			}
	
		}
		
		catch(SQLException e){
			System.out.println("Exception Handled while authenticating record.");
		}
		
		finally {
			try {
				refConnection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
			finally {
				System.out.println("Closing Connection..");
			}
		}
		
	}
}

				
				
